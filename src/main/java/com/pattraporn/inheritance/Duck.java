/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.inheritance;

/**
 *
 * @author Pattrapon N
 */
public class Duck extends Animal {
    private int numOfwings;
    public Duck(String name,String color){
        super(name,color,2);
        System.out.println("Duck created");
    }
   

    public void fly() {
         System.out.println("Duck: "+name+" fly!!!");
    }
}