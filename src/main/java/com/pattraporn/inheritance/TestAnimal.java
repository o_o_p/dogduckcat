/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.inheritance;

/**
 *
 * @author Pattrapon N
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "BlacknWhite");
        dang.speak();
        dang.walk();

        Dog2 mome = new Dog2("Mome", "Black&White");
        mome.speak();
        mome.walk();

        Dog3 bat = new Dog3("Bat", "Black&White");
        bat.speak();
        bat.walk();

        Dog4 to = new Dog4("To", "Black&White");
        to.speak();
        to.walk();
        
        Cat zero = new Cat("zero", "Orange");
        zero.speak();
        zero.walk();

        Duck zom = new Duck("zom", "Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Duck2 gabgab = new Duck2("GabGab", "Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
       
        System.out.println("zom is Animal: " + (zom instanceof Animal));
        System.out.println("zom is Duck: " + (zom instanceof Duck));
        System.out.println("zom is Cat: " + (zom instanceof Object));
        System.out.println("zom is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));

        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = zom;
        ani2 = zero;

        Animal[] animals = {dang, mome, bat, to, zero, zom, gabgab};
        for (int i = 0; i < animals.length; i++) {
             animals[i].walk();
            animals[i].speak();
           if (animals[i] instanceof Duck) {
               Duck duck = (Duck) animals[i];
                duck.fly();
            }
         }
    }
}
